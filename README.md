# Bootswitch

## Arcitecture

### Layouts 

* index.php
* front-page.php 
* single-post.php (article)
* page.php
* 404.php
* index.php

Layouts that call for:
* header.php
* footer.php

### Functions

functions.php calls:

* autoload.php ==> directly include all classes in the "classes" folder

* AdminTheme.php (theme settings)
* BsMenu.php (bootstrap menu)
* BSWidgets.php (sidebar, ...)
* GetBootswatch.php (CDN bootswatch)
* GetJson.php

### Settings.php

All files of type Json to be called either in Javascript or in PHP for example.

* bootswatch.json (with label, url, integrity, theme dark or not, ...)
