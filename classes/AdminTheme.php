<?php

class AdminTheme {

  const BOOTSWITCH = 'admintheme_options';

  public static function register () {

    add_action('admin_menu', [self::class, 'addMenu' ]);
    add_action('admin_init', [self::class, 'registerSettings' ]);
  }

  public static function registerSettings () {

    register_setting(self::BOOTSWITCH, 'bootswitch_choice', ['default' => 'cosmo']);

    add_settings_section('bootswitch_options_sections', 'Paramètres du thème ', function () {

      echo 'Sélectionnez le thème Bootswatch, puis enregistrez le !';

    }, self::BOOTSWITCH);

    add_settings_field('bootswitch_choice', 'Choix du '. ucfirst(BW), function () {
      ?>

      <fieldset>
        <?php

          $items = BOOTSWATCH;

          $list_radios = '';

          foreach( $items as $key => $theme ) {

            $checked = ( $key == get_option( 'bootswitch_choice' ) ) ? ' checked' : '';

            $link = ( $key == 'bootstrap') ? 'https://getbootstrap.com/' : 'https://bootswatch.com/'. $key;
            $dark = ( $theme['dark'] ) ? ' (dark theme)' : '';
      
            $list_radios.=  '<div>
              <input type="radio" id="'. $key .'" name="bootswitch_choice" value="'. $key .'"'. $checked .' />
              <label for="'. $key .'">
                <a target="_blank" rel="noreferrer noopener" href="' . $link .'" >' . $theme['label'] .'</a> 
                '. $dark .'
              </label>
            </div>';
          }
      
          echo '<div>' . $list_radios . '</div>';

        ?>
      </fieldset>
      <?php
    }, self::BOOTSWITCH, 'bootswitch_options_sections');
  }

  public static function addMenu () {

    add_theme_page('Gestion de '. ucfirst(BSW), 'Configuration de '. ucfirst(BSW), 'manage_options', self::BOOTSWITCH, [self::class, 'render' ]);
  }

  public static function render () { ?>

    <h1>Gestion de <?php echo ucfirst(BSW) ?></h1>

    <form action="options.php" method="post">

      <?php 
      
        settings_fields(self::BOOTSWITCH);
        do_settings_sections(self::BOOTSWITCH);
        submit_button() 
      
      ?>

    </form>

  <?php }

}