<?php

class BsMenu {

  public function __construct() {

    add_action( 'init', [ $this, 'bootswitch_menu' ] );
    add_filter('nav_menu_css_class', [ $this, 'add_additional_class_on_li' ], 1, 3);
    add_filter( 'nav_menu_link_attributes', [ $this, 'add_specific_menu_location_atts' ], 10, 3 );
	}

  public function bootswitch_menu () {  
    register_nav_menu( 'main-menu', 'Menu principal' );
  }
  
  public function add_additional_class_on_li ( $classes, $item, $args) {
    
    if( $args->menu == 'main-menu' ) {
      
      if(isset($args->add_li_class)) {
        $classes[] = $args->add_li_class;
      }
    }
    return $classes;
  }
  
  public function add_specific_menu_location_atts ( $atts, $item, $args ) {
    
    if( $args->menu == 'main-menu' ) {
      
      $class = 'nav-link';
      
      if ( $atts['aria-current'] == 'page'  ){
        $class.= ' active';
      }
      
      $atts['class'] = $class;
    }
    return $atts;
  }

}