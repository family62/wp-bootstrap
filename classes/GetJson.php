<?php

class GetJson {

  public function php_convert( $json_url ) {  

    $json = file_get_contents( $json_url );

    // See : https://www.php.net/manual/en/recursivearrayiterator.getchildren.php
    $jsonIterator = new RecursiveIteratorIterator(

      new RecursiveArrayIterator( json_decode( $json, TRUE ) ),
      RecursiveIteratorIterator::SELF_FIRST
    );

    foreach ($jsonIterator as $key => $row) {  
      
      if( is_array($row)) { $items[ $key ] = $row; }
    }

    return $items;
  }
}

?>