<?php

class BsWidgets {

  public function __construct() {

    add_action( 'widgets_init', [ $this, 'bootswitch_register_widget' ] );
	}

  public function bootswitch_register_widget() {

    register_sidebar(
      [
        'id' => 'article',
        'name' => 'Sidebar (Article)',
        'before_widget' => '<div class="p-4 mb-3 bg-light rounded %2$s" id="%1$s">',
        'after_widget' => '</div>',
      ]
    );
    register_sidebar(
      [
        'id' => 'footer1',
        'name' => 'Footer (Col 1)',
        'before_widget' => '<div id="%1$s">',
        'after_widget' => '</div>',
      ]
    );
    register_sidebar(
      [
        'id' => 'footer2',
        'name' => 'Footer (Col 2)',
        'before_widget' => '<div id="%1$s">',
        'after_widget' => '</div>',
      ]
    );
    register_sidebar(
      [
        'id' => 'footer3',
        'name' => 'Footer (Col 3)',
        'before_widget' => '<div id="%1$s">',
        'after_widget' => '</div>',
      ]
    );
    register_sidebar(
      [
        'id' => 'footer4',
        'name' => 'Footer (Col 4)',
        'before_widget' => '<div id="%1$s">',
        'after_widget' => '</div>',
      ]
    );
  }

}