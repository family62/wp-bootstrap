<?php

class GetBootswatch {

  protected $theme = [];
  protected $bootswatch = [];

  public function __construct() {

    add_action( 'after_setup_theme', [ $this, 'bootswitch_supports' ] );
    add_action( 'wp_enqueue_scripts', [ $this, 'bootswitch_assets' ] );
    //add_filter( 'style_loader_tag', [ $this, 'bootswitch_cdn_css_attributes' ], 10, 2 );
    add_filter( 'script_loader_tag', [ $this, 'bootswitch_cdn_js_attributes' ], 10, 2 );
	}

  public function bootswitch_supports () {
  
    add_theme_support ('title-tag'); // Add dynamic <title />
    add_theme_support ('post-thumbnails');
  }

  public function bootswitch_assets () {

    $this->get( get_option( 'bootswitch_choice' ) );
    extract( $this->theme );

    /**
     * 
     * Bootstrap: https://getbootstrap.com/
     * Bootswatch: https://bootswatch.com/
     * Boostrap icons: https://icons.getbootstrap.com/
     * 
     */
  
    # REGISTER
    # ------------------
    // Style
    wp_register_style( BS, $url );
    wp_register_style( 'bootstrap-icons', 'https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css'); 
    wp_register_style( 'bootswitch', get_template_directory_uri() . '/style.css', [BS]);
    // Script
    wp_register_script( BS, 'https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js', [], false, true);
  
    # ENQUEUE
    # ------------------
    // Style
    wp_enqueue_style( BS );
    wp_enqueue_style('bootstrap-icons');
    wp_enqueue_style('custom');
    // Script
    wp_enqueue_script( BS );
  }
  
  public function bootswitch_cdn_css_attributes ( $html, $handle ) {

    if ( BS === $handle ) {

      extract($this->theme);

      $replace = "integrity='$integrity'";
      $replace.= " crossorigin='$crossorigin'";
      $replace.= ($referrerpolicy != null ) ?: " referrerpolicy='$referrerpolicy";
      $replace = " crossorigin='anonymous'";

      return str_replace( "id=", "$replace id=", $html );
    }
    return $html;
  }
  
  // Avoiding Bootstrap JS duplication
  public function bootswitch_cdn_js_attributes ( $html, $handle ) {
  
    if ( BS === $handle ) {
      return str_replace( "id=", "integrity='sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p' crossorigin='anonymous' id=", $html );
    }
    return $html;
  }

  /**
   * -----------------------------------------------------------------------------------------------------
   * Bootswatch theme selection by key
   */
  public function get( $bootswatch_choice = 'bootstrap' ) {

    $bootswatch_choice = strtolower( $bootswatch_choice );

    // List of available Bootswatch themes (Array: $bootswatch) 
    //include 'config/bootswatch.php';
    $this->bootswatch = $bootswatch = BOOTSWATCH;

    // If the key does not correspond to any Bootswatch
    if ( !array_key_exists( $bootswatch_choice, $bootswatch ) ) {  $bootswatch_choice = 'bootstrap'; }

    $this->theme = $bootswatch[ $bootswatch_choice ];

    return $bootswatch_choice;
  }

  /**
   * -----------------------------------------------------------------------------------------------------
   * In case the Key function has not been called before
   */
  public function init () {

    if ( count( $this->theme ) == 0 ) { $this->get(); } 
  }

  /**
   * -----------------------------------------------------------------------------------------------------
   * Get the Bootswatch theme label
   */
  public function label () { 

    $this->init();
    return $this->theme['label'];
  }  

  /**
   * -----------------------------------------------------------------------------------------------------
   * Get the list of boostrap themes
   */
  public function listing () {

    $get_bootswatch = BOOTSWATCH;

    $listing = 'Labels with <span class="badge bg-secondary">ID</span><ul class="list-group list-group-flush mt-3">';

    foreach( $get_bootswatch as $key => $theme ) {

      $link = ( $key == 'bootstrap') ? 'https://getbootstrap.com/' : 'https://bootswatch.com/'. $key;
      $dark = ( $theme['dark'] ) ? '<span class="badge bg-dark">dark theme</span>' : '';

      $listing.=  '<li class="list-group-item">
        <a target="_blank" rel="noreferrer noopener" href="' . $link .'" >' . $theme['label'] .'</a>  
          <small class="exp"><i class="bi bi-box-arrow-up-right"></i></small>
        <span class="badge bg-secondary">'. $key .'</span> '. $dark .'
      </li>';
    }

    $listing.= '</ul>';

    return $listing;
  }
}