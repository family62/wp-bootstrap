<?php get_header() ?>

<div class="container">

  <?php if(have_posts()): while(have_posts()) : the_post(); ?>
    <article>

      <h1 class="blog-post-title"><?php the_title(); ?></h1>

      <picture>
        <img src="<?php the_post_thumbnail_url()?>" class="img-fluid" alt="" />
      </picture>
    
    </article>
  <?php endwhile; endif;?> 

</div>

<?php get_footer() ?>