<?php get_header() ?>

<i class="bi bi-arrow-left"></i> <a href="<?php echo get_site_url(); ?>">Retour accueil</a>
<div class="text-center">

  <h1 class="mt-3 mb-3">Page introuvable (Erreur 404)</h1>

  <p class="mb-5">La page que vous demandez n'existe pas ou a été supprimée...</p>

    <picture>
      <img 
        id="gif-404" 
        class="img-fluid-img-thumbnail rounded mx-auto d-block" 
        src="https://media4.giphy.com/media/bk8UGCysurqC2gmJ0o/200w.webp?cid=dda24d50acbc5996f52181349c7dcb1816bff0abad5e65b8&amp;rid=200w.webp&amp;ct=g" 
        srcset="https://media4.giphy.com/media/bk8UGCysurqC2gmJ0o/200w.webp?cid=dda24d50acbc5996f52181349c7dcb1816bff0abad5e65b8&amp;rid=200w.webp&amp;ct=g 200w,https://media4.giphy.com/media/bk8UGCysurqC2gmJ0o/giphy.webp?cid=dda24d50acbc5996f52181349c7dcb1816bff0abad5e65b8&amp;rid=giphy.webp&amp;ct=g 480w," 
        sizes="100vw" 
        alt=""
        style="width:30%" />
    </picture>
</div>

<?php get_footer() ?>