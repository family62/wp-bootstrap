<?php get_header() ?>

<div class="container">

  <div class="row g-5">

    <?php if(have_posts()): while(have_posts()) : the_post(); ?>
    <div class="col-md-8">

      <article class="blog-post">

        <h1 class="blog-post-title"><?php the_title(); ?></h1>
        <p class="blog-post-meta">
          <i class="bi bi-clock"></i> Publié le <?php the_time('d/m/Y'); ?>
        </p>

        <picture>
          <img src="<?php the_post_thumbnail_url()?>" class="img-fluid" alt="" />
        </picture>
      
      </article>
    </div>
    <?php endwhile; endif;?>
    
    <div class="col-md-4">

      <?php get_sidebar('article') ?>

      </div>   
    </div>

  </div>

</div>

<?php get_footer() ?>