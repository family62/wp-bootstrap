<?php get_header() ?>

<?php while(have_posts()) : the_post(); ?>
<section class="py-4 text-center container">
  <div class="row py-lg-5">
    <div class="col-lg-6 col-md-8 mx-auto">
      <h1 class="fw-light"><?php the_title(); ?></h1>
      <p class="lead text-muted"><?php the_content(); ?></p>
      <p>
        <a href="<?php echo get_post_type_archive_link('post') ?>" class="btn btn-primary my-2">Lire les articles</a>
      </p>
    </div>
  </div>
</section>
<?php endwhile; ?>

<div class="album py-5 bg-light">

  <div class="container">
    <div class="row">

    <?php 

      $latest_blog_posts = new WP_Query( array( 'posts_per_page' => 3 ) );
    
      if ( $latest_blog_posts->have_posts() ) : while ( $latest_blog_posts->have_posts() ) : $latest_blog_posts->the_post(); ?>

      <div class="col-md-4 col-sm-6 col-xs-12">

        <article>    
          <div class="card mb-5">
            <?php if (has_post_thumbnail( $post->ID ) ): ?>
              <?php the_post_thumbnail('post-thumbnail', 
                      [
                        'class' => 'card-img-top', 
                        'alt' => '',
                        'style' => 'height:auto;'
                      ]) ?>
            <?php else: ?>
              <img class="" src="https://place-hold.it/500x300" alt="" />
            <?php endif; ?>
            <div class="card-body">
              <h3 class="card-title"><?php the_title(); ?></h3>
              <p class="card-text">
                <i class="bi bi-clock"></i> Publié le <?php the_time('d/m/Y'); ?>
                <br /><?php if(!is_page()) : ?><?php the_category(', '); ?><?php endif; ?>
              </p>
              <p class="card-text">
                <?php if(is_singular()) : ?>
                  <?php the_content(); ?>
                <?php else : ?>
                  <?php the_excerpt(); ?>
                <?php endif; ?>
                  <span class="d-flex justify-content-center">
                  <a class="btn btn-primary" href="<?php the_permalink(); ?>">Lire la suite</a>
                  </span>
                
              </p>
            </div>
          </div>    
        </article>

      </div>

      <?php endwhile; endif; ?>
      </div>
  </div>
</div>

<?php get_footer() ?>