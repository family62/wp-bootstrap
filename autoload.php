<?php 

  spl_autoload_register( function( $classes ) {

    $base = get_template_directory() . '/classes/';
    $file = $base . $classes . '.php';

    if (file_exists( $file )) { require_once $file; }
 
  });

?>