<?php

namespace App;


define('BS', 'bootstrap');
define('BW', 'bootswatch');
define('BSW', 'bootswitch');

// Load Bootswitch Classes
require_once 'autoload.php';

use GetJson;
use GetBootswatch;
use BsMenu;
use BsWidgets;
use AdminTheme;


// See: wp_includes/general_templates.php

/**
* --------------------------------------------------------------------------
* STYLE
*/

$json = New GetJson();
define('BOOTSWATCH', $json->php_convert( get_template_directory() .'/settings/bootswatch.json' ));

New GetBootswatch();

/**
* --------------------------------------------------------------------------
* MENU
* 
* IMPORTANT = A menu must be defined by the webmaster
*
*/

New BsMenu();

/**
* --------------------------------------------------------------------------
* WIDGETS
*/

New BsWidgets();


/**
* --------------------------------------------------------------------------
* OPTIONS (THEME)
*/

AdminTheme::register();