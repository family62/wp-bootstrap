    </main>
  </div>

  <footer id="footer-main">

    <section id="before-footer" class="card">
      <div class="container px-4 py-5 my-3">
        <div class="row">

          <?php if(is_active_sidebar('footer1') ) { ?> 
            <div id="footer1" class="col footer">
              <?php get_sidebar('footer1'); ?>
            </div>
          <?php } ?>
          <?php if(is_active_sidebar('footer2') ) { ?> 
            <div id="footer2" class="col footer">
              <?php get_sidebar('footer2'); ?>
            </div>
          <?php } ?>
          <?php if(is_active_sidebar('footer3') ) { ?> 
            <div id="footer3" class="col footer">
              <?php get_sidebar('footer3'); ?>
            </div>
          <?php } ?>
          <?php if(is_active_sidebar('footer4') ) { ?> 
            <div id="footer4" class="col footer">
              <?php get_sidebar('footer4'); ?>
            </div>
          <?php } ?>

        </div>
      </div>
    </section>
    <section class="container text-center">
      <div class="container mt-3 mb-3">  
        &copy; <?php echo date('Y'); ?> <strong><a href="<?php echo get_site_url(); ?>"><?php echo bloginfo('name'); ?></a></strong> 
      </div>
    </section>

  </footer>

  <p class="text-center">
    <?php 
      echo BW .' = '; 
      echo get_option( 'bootswitch_choice' );
    ?>
  </p>

  <?php wp_footer() ?>

</body>
</html>