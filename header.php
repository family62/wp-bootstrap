<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_head() ?>
</head>
<body>

  <header id="header-main">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container">

        <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
          <?php echo bloginfo('name'); ?>
        </a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>

        <!-- <div class="collapse navbar-collapse" id="main-menut">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="#">Home</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#">Link</a>
            </li>
          </ul>
          <form class="d-flex">
            <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success" type="submit">Search</button>
          </form>
        </div> -->

        <?php

          $args = [
            'container'       => 'div',
            'container_id'    => 'main-menu',
            'container_class' => 'collapse navbar-collapse',
            'menu'            => 'main-menu',
            'menu_id'         => 'primary-menu',
            'menu_class'      => 'navbar-nav',
            'depth'           =>  1, // 1 = no dropdowns, 2 = with dropdowns.
            'add_li_class'    => 'nav-item'
          ];
          wp_nav_menu( $args ); 

        ?>

      </div>
    </nav>
  </header>

  <div id="site-content" class="mt-5">