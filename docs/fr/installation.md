# MEMO installation

Pour installer ce projet, il faut en résumé :

1 - Lancer un serveur Web (en local ou en ligne) 
2 - Créer une base de données
3 - Installer Wordpress
4 -  Ajouter le dossier du thème “bootswitch”

En détail :

## 1 - LAMP

Wordpress fonctionne sous un environnement LAMP (Linux Apache, MySQL, PHP).

Il est possible d’installer un LAMP directement sous Linux naturellement (ou WSL2 sous Windows par exemple). On y trouve donc : 

* un serveur HTTP (comme Apache, Nginx ou Caddy)
* une base de données MySQL ou MariaDB
* PHP (version supérieure ou égale à 7.4 aujourd’hui)

### Alternatives

On peut aussi plus simplement utiliser des outils comme WampServer (Windows) ou Mamp (Mac).

Le mieux est d’installer Docker (avec un orchestrateur comme Traefik). C’est plus compliqué à apprendre, mais le bénéfice est excellent : on peut lancer rapidement un projet, y compris avec des architectures complexes. Et il est possible d’imiter l’environnement de production.

### Conf serveur du projet

Quelque soit la solution utilisée, il faut qu’une configuration du site web soit créée sur le serveur HTTP, du genre “monsiteweb.conf”, qui va pointer sur une adresse locale comme 127.0.0.1, généralement sur le port 80.

Note : il est conseillé de créer un faux nom de domaine, du type “monsite.dev” (qui sera associée à l’adresse IP locale).
 Attention, tout nom de domaine modifié doit être mis à jour dans la base de données de Wordpress.


## 2 - Gérer la base

Pour simplifier la création et la gestion de la base, on peut utiliser une interface graphique via “adminer.php” (simple à installer) ou “phpyMyAdmin”.


## 3 - Installer Wordpress, puis, de préférence :

Créer 2 à 3 faux articles (dont au moins un avec une image de couverture)
Créer 2 pages :

* 1 page pour l’accueil
* 1 page pour la liste des articles
* Associer ces pages dans le réglage de Wordpress
* Créer au moins un menu principal


## 4 - Ajouter ce thème

Il suffit alors d'ajouter le thème manuellement ou via git dans le dossier des thèmes de Wordpress, exemple :

==> /var/www/wordpress/wp-content/themes

Dans le cas d'une acquisition via git, on peut cloner un projet via :

```git clone [adresse_du_dépôt] [nom_du_dossier_souhaité => optionnel]```

Puis se placer dans la branche via un git checkout [branche_souhaitée]

Il n'y a plus qu'à activer le thèmme.

On peut éventuellement ajouter des widgets, etc.