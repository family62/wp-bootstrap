<?php get_header() ?>

<div class="container">

<?php if(have_posts()): ?>
  <div class="wp-loop row">

    <?php while(have_posts()) : the_post(); ?>
    <div class="col-md-4 col-sm-6 col-xs-12">

      <article>    
        <div class="card mb-5">
          <?php if (has_post_thumbnail( $post->ID ) ): ?>
            <?php the_post_thumbnail('post-thumbnail', 
                    [
                      'class' => 'card-img-top', 
                      'alt' => '',
                      'style' => 'height:auto;'
                    ]) ?>
          <?php else: ?>
            <img class="" src="https://place-hold.it/500x300" alt="" />
          <?php endif; ?>
          <div class="card-body">
            <h3 class="card-title"><?php the_title(); ?></h3>
            <p class="card-text">
              <i class="bi bi-clock"></i> Publié le <?php the_time('d/m/Y'); ?>
              <br /><?php if(!is_page()) : ?><?php the_category(', '); ?><?php endif; ?>
            </p>
            <p class="card-text">
              <?php if(is_singular()) : ?>
                <?php the_content(); ?>
              <?php else : ?>
                <?php the_excerpt(); ?>
                <span class="d-flex justify-content-center">
                <a class="btn btn-primary" href="<?php the_permalink(); ?>">Lire la suite</a>
                </span>
              <?php endif; ?>
            </p>
          </div>
        </div>    
      </article>

    </div>
    <?php endwhile; ?>

  </div>

<?php else: ?>

  <p>Aucun article...</p>

<?php endif;?>

</div>

<?php get_footer() ?>